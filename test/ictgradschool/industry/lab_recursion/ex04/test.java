package ictgradschool.industry.lab_recursion.ex04;

public class test {

    private void start(){
        System.out.println(bar(3,-2));

    }

    private double bar(double x, int n) {
        if (n > 1)
            return x * bar(x, n - 1);
        else if (n < 0)
            return 1.0 / bar(x, -n);
        else
            return x;
    }


    public static void main(String[] args) {
        new test().start();
    }
}
